# List Annotator (LISA)

List Annotator (LISA) is a standalone and light-weight HTML/CSS/JavaScript based application to efficiently annotate a large list of images. LISA is an [open source project](https://gitlab.com/vgg/lisa) developed and maintained by the Visual Geometry Group ([VGG](https://www.robots.ox.ac.uk/~vgg/)) and released under a license that grants its users the freedom to use it for any purpose.

## Demo Video
 * [LISA Demo List Annotation](https://www.robots.ox.ac.uk/~vgg/software/via/_tmp/lisa/lisa_demo.mp4)
 * [NLS-Chapbooks Dataset Annotation using LISA](https://www.robots.ox.ac.uk/~vgg/software/via/_tmp/lisa/lisa_nls_chapbooks.mp4)

## Download
 * [lisa.html](https://gitlab.com/vgg/lisa/uploads/752e9a6b432d80f6212d0cf6011ad9f5/lisa.html) : Open `lisa.html` file in a web browser to load the List Annotator (LISA) application.
 * Source code : see the [releases](https://gitlab.com/vgg/lisa/-/releases/) page.

## Generating Self-Contained `lisa.html` application
```
python scripts/pack_lisa_html.py
# this will generate dist/lisa.html which can be shared or used in any web browser
```
## LISA Project File Description

```
{
  "project": {
    "shared_fid":"__FILE_ID__",       # used for VGG shared project store
    "shared_rev":"__FILE_REV_ID__",   # used for VGG shared project store
    "shared_rev_timestamp":"__FILE_REV_TIMESTAMP__",
    "project_name": "mynah_bird_singing",
    "project_id": "",
    "creator": "List Annotator - LISA (https://gitlab.com/vgg/lisa)",
    "editor": "List Annotator - LISA (https://gitlab.com/vgg/lisa)",
    "file_format_version": "0.0.3",
    "created_timestamp": 1601656344.9172015
  },
  "config": {
    "file_src_prefix": "",   # this prefix gets prepended to all file src
    "navigation_from": -1,
    "navigation_to": -1,
    "item_per_page": 100,    # number of images or videos shown per page
    "float_precision": 4,
    "show_attributes": {     # the attributes that are visible to the user
      "file": [],
      "region": [
        "class"
      ]
    }
  },
  "attributes": {
    "file": {
      "width": {
        "aname": "Width",
        "atype": "text"      # can be {label, text, checkbox, radio} for file attributes
      },
      "height": {
        "aname": "Height",
        "atype": "text"
      }
    },
    "region": {
      "class": {
        "aname": "High Level Class",
        "atype": "select",   # can be {select, radio, checkbox, label} for region attributes
        "options": {
          "face": "Face (people talking/singing)",
          "human": "Human",
          "music": "Music (instrument)",
          "animals": "Animals",
          "sound_things": "Sounds of things (tools, machine, ground)",
          "natural_sounds": "Natural sounds (wind, rain, storm)",
          "environment": "Environment (crowd, background noise)"
        }
      }
    }
  },
  "files": [
    {
      "fid": "10",            # unique file identifier
      "src": "Q5zz22v.jpg",   # file gets loaded from file_src_prefix + src
      "regions": [            # an array of rectangular regions
        [                     # first region
          77.74,              # x
          11.95,              # y
          98.072,             # width
          102.856             # height
        ],
        [                     # second region
          64.584,
          116.012,
          84.916,
          88.50
        ],
        [                     # third region
          236.808,
          95.67,
          58.604,
          117.208
        ]
      ],
      "rdata": [              # an array of metadata for each region
        {
          "class": "animals"
        },
        {
          "class": "animals"
        },
        {
          "class": "animals"
        }
      ],
      "fdata": {             # metadata for file
        "width": 299,
        "height": 299
      }
    },
    { ... }
  ]
}
```
## Contact
The LISA project is developed and maintained by [Abhishek Dutta](mailto:adutta-REMOVE@robots.ox.ac.uk) and [David Miguel Susano Pinto](mailto:pinto@robots.ox.ac.uk). You are welcome to send your queries and feedback to the maintainers by email or through the [issues](https://gitlab.com/vgg/lisa/-/issues) portal.
